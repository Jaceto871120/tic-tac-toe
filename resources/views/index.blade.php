@extends('layouts.master')
@section('link')

@endsection
@section('header')

@endsection
@section('content')
<div class="col-md-12">
  @if (session()->has('info'))

    <div class="alert alert-danger">{{session('info')}}</div>

  @endif
</div>
<div class="container">

  <div class="row justify-content-center">
      <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                <h1>TIC-TAC-TOE</h1>
              </div>

              <div class="card-body">
              	<a href="" class="btn btn-primary" data-toggle="modal" data-target="#crearPartida"><i class="fas fa-plus-circle">Nueva Partida</i></a>
                <a href="" class="btn btn-success" data-toggle="modal" data-target="#cargarPartida"><i class="fas fa-sync-alt">Cargar Partida</i></a>

                	{{-- Modal Crear Partida --}}
                <div class="modal" id="crearPartida" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header d-flex justify-content-center">
                              <h5 class="modal-title">Crear Nueva Partida</h5>
                            </div>
                            <div class="modal-body">
                            	<form action="{{route('partida.nuevaPartida')}}" method="post">
                                  @csrf
                                <div class="form-group">
                                	<label>Jugador1</label>
				                    <input type="text" class="form-control" name="jugador1" value="{{ old('jugador1') }}" placeholder="Jugador1">
				                    {!! $errors->first('jugador1')!!}
				                 </div>
				                 <div class="form-group">
                                	<label>Jugador2</label>
				                    <input type="text" class="form-control" name="jugador2" value="{{ old('jugador2') }}" placeholder="Jugador2">
				                    {!! $errors->first('jugador2')!!}
				                 </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">


                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Crear</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!--fin modal-->

                {{-- Modal Cargar Partida --}}
				<div class="modal" id="cargarPartida" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header d-flex justify-content-center">
                              <h5 class="modal-title">Ingrese el ID de la partida</h5>
                            </div>
                            <div class="modal-body">
                            	<form action="{{route('partida.cargarPartida')}}" method="post">
                                  @csrf
                                <div class="form-group">
				                    <input type="number" class="form-control" name="id" value="{{ old('id') }}" required>
				                    {!! $errors->first('id')!!}
				                 </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">


                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Cargar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!--fin modal-->



              </div>
          </div>
      </div>
  </div>

</div>
@endsection
@section('scripts')

@endsection



