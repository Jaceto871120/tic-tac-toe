@extends('layouts.master')
@section('link')

@endsection
@section('header')

@endsection
@section('content')
<div class="col-md-12">
  @if (session()->has('info'))

    <div class="alert alert-success">{{session('info')}}</div>

  @endif
</div>
<div class="container">

  <div class="row justify-content-center">
      <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                <h1>Partida No.{{$partida->id}}</h1>
              </div>

              <div class="card-body">
              	Turno del jugador:
              	@if($partida->turno==0)
              	{{$partida->jugador1}}
                <a href="" class="btn btn-primary" data-toggle="modal" data-target="#editarJugador"><i class="fas fa-user-edit">Cambiar nombre</i></a>
              	@else
              	{{$partida->jugador2}}
                <a href="" class="btn btn-primary" data-toggle="modal" data-target="#editarJugador"><i class="fas fa-user-edit">Cambiar nombre</i></a>
              	@endif

              	<table>
              		<tr>
              			<th></th>
              			<th>1</th>
              			<th>2</th>
              			<th>3</th>
              		</tr>
              		<tr>
              			<th>A</th>
                    @if($partida->a1==null)
                    <th><a class="btn btn-secondary" href="{{route('partida.cambiar',[$partida->id,'a1'])}}"></a></th>
                    @else
              			@if($partida->a1==0)
              			<th><button type="button" class="btn btn-primary" disabled>O</button></th>
              			@else
              			<th><button type="button" class="btn btn-primary" disabled>X</button></th>
              			@endif
              			@endif


              			@if($partida->a2==null)
              			<th><a class="btn btn-secondary" href="{{route('partida.cambiar',[$partida->id,'a2'])}}"></a></th>
              			@else
              			@if($partida->a2==0)
              			<th><button type="button" class="btn btn-primary" disabled>O</button></th>
              			@else
              			<th><button type="button" class="btn btn-primary" disabled>X</button></th>
              			@endif
              			@endif

              			@if($partida->a3==null)
              			<th><a class="btn btn-secondary" href="{{route('partida.cambiar',[$partida->id,'a3'])}}"></a></th>
              			@else
              			@if($partida->a3==0)
              			<th><button type="button" class="btn btn-primary" disabled>O</button></th>
              			@else
              			<th><button type="button" class="btn btn-primary" disabled>X</button></th>
              			@endif
              			@endif
              		</tr>
              		<tr>
              			<th>B</th>
              			@if($partida->b1==null)
              			<th><a class="btn btn-secondary" href="{{route('partida.cambiar',[$partida->id,'b1'])}}"></a></th>
              			@else
              			@if($partida->b1==0)
              			<th><button type="button" class="btn btn-primary" disabled>O</button></th>
              			@else
              			<th><button type="button" class="btn btn-primary" disabled>X</button></th>
              			@endif
              			@endif

              			@if($partida->b2==null)
              			<th><a class="btn btn-secondary" href="{{route('partida.cambiar',[$partida->id,'b2'])}}"></a></th>
              			@else
              			@if($partida->b2==0)
              			<th><button type="button" class="btn btn-primary" disabled>O</button></th>
              			@else
              			<th><button type="button" class="btn btn-primary" disabled>X</button></th>
              			@endif
              			@endif

              			@if($partida->b3==null)
              			<th><a class="btn btn-secondary" href="{{route('partida.cambiar',[$partida->id,'b3'])}}"></a></th>
              			@else
              			@if($partida->b3==0)
              			<th><button type="button" class="btn btn-primary" disabled>O</button></th>
              			@else
              			<th><button type="button" class="btn btn-primary" disabled>X</button></th>
              			@endif
              			@endif
              		</tr>
              		<tr>
              			<th>C</th>
              			@if($partida->c1==null)
              			<th><a class="btn btn-secondary" href="{{route('partida.cambiar',[$partida->id,'c1'])}}"></a></th>
              			@else
              			@if($partida->c1==0)
              			<th><button type="button" class="btn btn-primary" disabled>O</button></th>
              			@else
              			<th><button type="button" class="btn btn-primary" disabled>X</button></th>
              			@endif
              			@endif

              			@if($partida->c2==null)
              			<th><a class="btn btn-secondary" href="{{route('partida.cambiar',[$partida->id,'c2'])}}"></a></th>
              			@else
              			@if($partida->c2==0)
              			<th><button type="button" class="btn btn-primary" disabled>O</button></th>
              			@else
              			<th><button type="button" class="btn btn-primary" disabled>X</button></th>
              			@endif
              			@endif

              			@if($partida->c3==null)
              			<th><a class="btn btn-secondary" href="{{route('partida.cambiar',[$partida->id,'c3'])}}"></a></th>
              			@else
              			@if($partida->c3==0)
              			<th><button type="button" class="btn btn-primary" disabled>O</button></th>
              			@else
              			<th><button type="button" class="btn btn-primary" disabled>X</button></th>
              			@endif
              			@endif
              		</tr>
              	</table>

              	<div class="col-md-12">
				  @if ($partida->terminada == true)

				    <div class="alert alert-success">Juego terminado Ganador @if($partida->turno==1)
	              	{{$partida->jugador1}}
	              	@else
	              	{{$partida->jugador2}}
	              	@endif
	              </div>

				    <a class="btn btn-success" href="{{route('partida.reiniciar',$partida->id)}}"><i class="fas fa-sync-alt"> Reiniciar Partida</i></a>

				  @endif
				  @if ($partida->terminada == false and ($partida->a1!=null and $partida->a2!=null and $partida->a3!=null and $partida->b1!=null and $partida->b2!=null and $partida->b3!=null and $partida->c1!=null and $partida->c2!=null and $partida->c3!=null))

				    <div class="alert alert-primary">Juego terminado Empatado</div>

				    <a class="btn btn-primary" href="{{route('partida.reiniciar',$partida->id)}}"><i class="fas fa-sync-alt"> Reiniciar Partida</i></a>

				  @endif
				</div>
              </div>
          </div>
      </div>
  </div>

    {{-- Modal Crear Partida --}}
    <div class="modal" id="editarJugador" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-center">
                  <h5 class="modal-title">Cambiar Nombre</h5>
                </div>
                <div class="modal-body">
                  <form action="{{route('partida.actualizarJugador')}}" method="post">
                      @csrf
                    <div class="form-group">
                      <label>Nombre</label>
                      @if($partida->turno==0)
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{$partida->jugador1}}" required>
                      {!! $errors->first('nombre')!!}
                      @else
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{$partida->jugador2}}" required>
                      {!! $errors->first('nombre')!!}
                      @endif
                   </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">

                        <input type="hidden" name="turno" value="{{$partida->turno}}">
                        <input type="hidden" name="id" value="{{$partida->id}}">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </form>
                </div>
            </div>
        </div>
    </div><!--fin modal-->

</div>
@endsection
@section('scripts')

@endsection



