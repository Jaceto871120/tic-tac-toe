<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/partidaNueva/', 'PartidaController@nuevaPartida')->name('partida.nuevaPartida');
Route::post('/cargarPartida', 'PartidaController@cargarPartida')->name('partida.cargarPartida');
Route::get('/partidas/{partida}', 'PartidaController@show')->name('partida.show');
Route::get('/marcar/{id}/{a}', 'PartidaController@marcar')->name('partida.cambiar');
Route::get('/partida', 'HomeController@marcar')->name('partida.marcar');
Route::get('/reiniciar/{id}', 'PartidaController@reiniciar')->name('partida.reiniciar');
Route::post('/actualizarJugador', 'PartidaController@actualizarJugador')->name('partida.actualizarJugador');
