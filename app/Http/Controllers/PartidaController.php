<?php

namespace App\Http\Controllers;

use App\Partida;
use Illuminate\Http\Request;

class PartidaController extends Controller
{
    // Funcion encargada de realizar la carga de las partidas y su estado actual
    public function show($id)
    {
        $partida = Partida::findOrFail($id);

        return view('partida', compact('partida'));
    }

    // Funcion encargada de crear nuevas partidas
    public function nuevaPartida(Request $request)
    {
        // dd($request);
        $partida = new Partida;
        if ($request->input('jugador1')!=null)
        {
            $partida->jugador1 = $request->input('jugador1');
        }
        else{
            $partida->jugador1 = 'Jugador1';
        }
        if ($request->input('jugador1')!=null)
        {
            $partida->jugador2 = $request->input('jugador2');
        }
        else{
            $partida->jugador2 = 'Jugador2';
        }
        $partida->inicial = 0;
        $partida->turno = 0;
        $partida->terminada = 0;
        $partida->save();

        return redirect()->route('partida.show',$partida->id);
    }
    //  Buscar la informacion de partidas iniciadas anteriormente, carga sus datos en la vista de partida
    public function cargarPartida(Request $request)
    {
        // dd($request);
        $partida = Partida::find($request->input('id'));

        if($partida==null){
            return back()->with('info', 'Partida no encontrada');
        }

        return redirect()->route('partida.show',$partida->id);
    }
    //  Se encarga de realizar el registro de los movimientos de cada jugador, lleva el conteo de turnos y valida si el juego ya fue ganado haciendo uso de la funcion comparar
    public function marcar($id, $a)
    {
        $partida = Partida::findOrFail($id);
        $partida->{$a}=$partida->turno;
        if($partida->turno==0){
            $partida->turno=1;
        }else{
            $partida->turno=0;
        }
        $partida->update();

        $this->comparar($id);

        return redirect()->route('partida.show',$partida->id);
    }
    //  La funcion comparar, revisa en cada movimiento que el juego no haya terminado verificando que no existan tres en linea.
    public function comparar($id)
    {
        $partida = Partida::findOrFail($id);


        if(($partida->a1 !=null and  $partida->a1 == $partida->a2 and $partida->a3==$partida->a2) or
            ($partida->b1 !=null and $partida->b1 == $partida->b2 and $partida->b3==$partida->b2 ) or
            ($partida->c1 !=null and $partida->c1 == $partida->c2 and $partida->c3==$partida->c2 ) or
            ($partida->a1 !=null and $partida->a1 == $partida->b1 and $partida->c1==$partida->b1 ) or
            ($partida->a2 !=null and $partida->a2 == $partida->b2 and $partida->c2==$partida->b2 ) or
            ($partida->a3 !=null and $partida->a3 == $partida->b3 and $partida->c3==$partida->b3 ) or
            ($partida->a1 !=null and $partida->a1 == $partida->b2 and $partida->c3==$partida->b2 ) or
            ($partida->a3 !=null and $partida->a3 == $partida->b2 and $partida->c1==$partida->b2 ))
        {
            $partida->terminada=1;
            $partida->update();

            return;
        }
        else{
            return;
        }

    }
    // Funcion encargada de devolver los valores de la partida a cero y reinicar la misma, no sin antes cambiar el turno de quien inicia moviendo en esta nueva partida.
    public function reiniciar($id)
    {
        $partida = Partida::findOrFail($id);
        if($partida->inicial==0){
            $partida->inicial=1;
        }else{
            $partida->inicial=0;
        }
        $partida->a1=null;
        $partida->a2=null;
        $partida->a3=null;
        $partida->b1=null;
        $partida->b2=null;
        $partida->b3=null;
        $partida->c1=null;
        $partida->c2=null;
        $partida->c3=null;
        $partida->turno=$partida->inicial;
        $partida->terminada=0;
        $partida->update();

        return redirect()->route('partida.show',$partida->id);
    }
    public function actualizarJugador(Request $request)
    {
        $partida = Partida::findOrFail($request->input('id'));
        if($request->input('turno')==0)
        {
            $partida->jugador1=$request->input('nombre');
            $partida->update();
        }else{
            $partida->jugador2=$request->input('nombre');
            $partida->update();
        }
        return back()->with('info', 'Juagador Actualizado');
    }
}
