<?php

namespace App;

use App\Jugador;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Partida extends Model
{
    use Notifiable;

    protected $fillable = [
        'inicial','jugador1','jugador2','a1','a2','a3','b1','b2','b3','c1','c2','c3','turno','terminada',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function jugador()
    {
        return $this->belongsTo(Jugador::class);
    }
}
